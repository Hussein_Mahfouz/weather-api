import React, { useState } from "react";
import "./App.css";
import Error from "./components/Error";
import IncomingWeather from "./components/IncomingWeather";
import Search from "./components/Search";
import { defCo } from "./components/utils/getColor";
import WeatherItem from "./components/WeatherItem";

const App = () => {
  let current, remaining;
  const [city, setCity] = useState("");
  const [data, setData] = useState("");
  const [error, setError] = useState(false);
  const [color, setColor] = useState(defCo);
  const fetchData = (e) => {
    e.preventDefault();
    const data = async () => {
      await fetch(
        `https://api.openweathermap.org/data/2.5/forecast?q=${city}&cnt=8&units=metric&appid=4459cccf26f5959f575f9ded555922bd`
      )
        .then((res) => {
          if (res.ok) {
            return res.json();
          } else {
            throw new Error("Something went wrong");
          }
        })
        .then((data) => {
          setData(data);
          setCity("");
          setError("");
        })
        .catch((err) => {
          setError(err);
        });
    };
    data();
  };
    // if (data) {
    //   current = data.list[0];
    //   remaining = data.list.slice(1);
    // }
  if (data !== "") {
    const { list } = data;
    [current, ...remaining] = list;
    console.log();
    console.log(list);
  }

  if (data === "")
    return (
      <div className="app" style={color[0]}>
        <Search setCity={setCity} onClick={fetchData} color={color} />
        {error ? <Error error={error} /> : ""}
        {data ? (
          <>
            <WeatherItem current={current} color={setColor} />
            <IncomingWeather remaining={remaining} />
          </>
        ) : (
          ""
        )}
      </div>
    );
  else
    return (
      <div className="app" style={color[0]}>
        <Search setCity={setCity} onClick={fetchData} color={color} />
        <WeatherItem current={current} color={setColor} />
        <IncomingWeather rem={remaining} />
        <></>
      </div>
    );
};

export default App;
