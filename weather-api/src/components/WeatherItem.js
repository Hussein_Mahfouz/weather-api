import React, { useEffect } from "react";
import { clearCo, defCo, rainCo } from "./utils/getColor";
import getImages from "./utils/getImages";

const WeatherItem = ({ current, color }) => {
  console.log(current);

  const humidity = current.main.humidity;
  const pressure = current.main.pressure;
  const description = current.weather[0].description;
  const minTempC = Math.round(current.main.temp_min);
  const maxTempC = Math.round(current.main.temp_max);

  useEffect(() => {
    if (current.weather[0].main === "Clear") color(clearCo);
    else if (current.weather[0].main === "Rain") color(rainCo);
    else color(defCo);
  });

  return (
    <div id="weather">
      <img src={getImages(current.weather[0].id)} alt="clear icon" />

      <p id="desc">{description}</p>
      <p id="temp">
        <b>Tempreture</b> {minTempC}°C to {maxTempC}
        °C
      </p>
      <p className="p2">
        <b>Humidity &nbsp;&nbsp;</b>
        {humidity}% &nbsp;&nbsp;{" "}
        <span id="pres">
          {" "}
          <b>Pressure&nbsp;&nbsp;</b>
        </span>
        {pressure}
      </p>

      <br></br>
      <br></br>
      <br></br>

      <div></div>
    </div>
  );
};

export default WeatherItem;
