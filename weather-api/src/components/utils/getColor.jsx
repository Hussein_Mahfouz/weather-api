export const defCo = [
  {
    backgroundColor: "#a4ccef",
  },
  {
    backgroundColor: "#6b90d7",
  },
];

export const clearCo = [
  {
    backgroundColor: "#1b56bd",
  },
  {
    backgroundColor: "#1a6cd4",
  },
];

export const rainCo = [
  {
    backgroundColor: "#a1b5bc",
  },
  {
    backgroundColor: "#7180b2",
  },
];
