import {
  default as clear,
  default as unknown,
} from "../../img/weather-icons/clear.svg";
import drizzle from "../../img/weather-icons/drizzle.svg";
import fog from "../../img/weather-icons/fog.svg";
import mostlycloudy from "../../img/weather-icons/mostlycloudy.svg";
import partlycloudy from "../../img/weather-icons/partlycloudy.svg";
import rain from "../../img/weather-icons/rain.svg";
import snow from "../../img/weather-icons/snow.svg";
import storm from "../../img/weather-icons/storm.svg";
const getImage = (id) => {
  let source = 0;
  if (id <= 300) {
    source = storm;
  } else if (id > 300 && id <= 499) {
    source = drizzle;
  } else if (id > 500 && id <= 599) {
    source = rain;
  } else if (id > 600 && id <= 699) {
    source = snow;
  } else if (id > 700 && id <= 799) {
    source = fog;
  } else if (id === 800) {
    source = clear;
  } else if (id === 801) {
    source = partlycloudy;
  } else if (id > 800 && id <= 805) {
    source = mostlycloudy;
  } else {
    source = unknown;
  }
  return source;
};

export default getImage;
